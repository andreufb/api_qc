import logging
import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
import traceback
from flask_restplus import Api
from api import settings
from sqlalchemy.orm.exc import NoResultFound

log = logging.getLogger(__name__)

api = Api(version='9.0', title='API for Quality Control',
          description='API for get, modify and add different type of camps related to the data, devices, persons, '
                      'projects and missions of the OBSEA and for applying real-time QC to the data')


@api.errorhandler
def default_error_handler(e):
    message = 'An unhandled exception occurred.'
    log.exception(message)

    if not settings.FLASK_DEBUG:
        return {'message': message}, 500


@api.errorhandler(NoResultFound)
def handle_no_result_exception(error):
    '''Return a custom not found error message and 404 status code'''
    return {'message': error.specific}, 404
