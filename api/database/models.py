# The examples in this file come from the Flask-SQLAlchemy documentation
# For more information take a look at:
# http://flask-sqlalchemy.pocoo.org/2.1/quickstart/#simple-relationships

from api.database import db


class Device(db.Model):
    __tablename__ = 'devices'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    definition = db.Column(db.Text, nullable=False)

    def __init__(self, name, definition):
        self.name = name
        self.definition = definition

    def __repr__(self, name):
        return '<Device %r>' % self.name


# class PhysicalSystems(db.Model):
# S'usara mes tard per adquirir les dades de les taules de dades i convertir-les en format JSON.

# class Persons(db.Model):
# S'usara per adquirir les dades de les taules de dades i convertir-ho a JSON.

# class Projects(db.Model):
# S'usara per adquirir les dades de les taules de dades i convertir-ho a JSON.

# class Missions(db.Model):
# S'usara per adquirir les dades de les taules de dades i convertir-ho a JSON.

# class Actions(db.Model):
# S'usara per adquirir les dades de les taules de dades i convertir-ho a JSON.

# class Locations(db.Model):


# class FeaturesOfInterest(db.Model):


# class ObservableProperties(db.Model):


# class UnitsOfMeasurement(db.Model):


# class Observations(db.Model):
