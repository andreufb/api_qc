from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def reset_database():
    from api.database.models import Device, Data
    db.drop_all()
    db.create_all()
