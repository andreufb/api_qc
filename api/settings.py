# Flask settings
FLASK_SERVER_NAME = '147.83.159.160:5000'
FLASK_DEBUG = True  # Do not use debug mode in production

# Flask-Restplus settings
RESTPLUS_SWAGGER_UI_DOC_EXPANSION = 'list'
RESTPLUS_VALIDATE = True
RESTPLUS_MASK_SWAGGER = False
RESTPLUS_ERROR_404_HELP = False

# SQLAlchemy settings
SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://andreu:4Ndr3u.4ndr3u@web.obsea.es:13307/dmp'
SQLALCHEMY_TRACK_MODIFICATIONS = False


from dotenv import load_dotenv
load_dotenv()