import logging.config
import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
import os
from flask import Flask, Blueprint
from api import settings
from api.blog.endpoints.auth import ns as blog_auth_namespace
from api.blog.endpoints.physicalSystems import ns as blog_physicalSystems_namespace
from api.blog.endpoints.persons import ns as blog_persons_namespace
from api.blog.endpoints.projects import ns as blog_projects_namespace
from api.blog.endpoints.missions import ns as blog_missions_namespace
from api.blog.endpoints.actions import ns as blog_actions_namespace
from api.blog.endpoints.location import ns as blog_locations_namespace
from api.blog.endpoints.featureOfInterest import ns as blog_featuresOfInterest_namespace
from api.blog.endpoints.observableProperty import ns as blog_observableProperties_namespace
from api.blog.endpoints.unitOfMeasurement import ns as blog_unitsOfMeasurement_namespace
from api.blog.endpoints.observation import ns as blog_observations_namespace
from api.restplus import api
from api.database import db
from flask_jwt_extended import JWTManager

app = Flask(__name__)
app.config.from_envvar('ENV_FILE_LOCATION')
jwt = JWTManager(app)
logging_conf_path = os.path.normpath(os.path.join(os.path.dirname(__file__), '../logging.conf'))
logging.config.fileConfig(logging_conf_path)
log = logging.getLogger(__name__)


def configure_app(flask_app):
    flask_app.config['SERVER_NAME'] = settings.FLASK_SERVER_NAME
    flask_app.config['SQLALCHEMY_DATABASE_URI'] = settings.SQLALCHEMY_DATABASE_URI
    flask_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = settings.SQLALCHEMY_TRACK_MODIFICATIONS
    flask_app.config['SWAGGER_UI_DOC_EXPANSION'] = settings.RESTPLUS_SWAGGER_UI_DOC_EXPANSION
    flask_app.config['RESTPLUS_VALIDATE'] = settings.RESTPLUS_VALIDATE
    flask_app.config['RESTPLUS_MASK_SWAGGER'] = settings.RESTPLUS_MASK_SWAGGER
    flask_app.config['ERROR_404_HELP'] = settings.RESTPLUS_ERROR_404_HELP


def initialize_app(flask_app):
    configure_app(flask_app)

    blueprint = Blueprint('api', __name__, url_prefix='/api')
    api.init_app(blueprint)
    api.add_namespace(blog_auth_namespace)
    api.add_namespace(blog_physicalSystems_namespace)
    api.add_namespace(blog_persons_namespace)
    api.add_namespace(blog_projects_namespace)
    api.add_namespace(blog_missions_namespace)
    api.add_namespace(blog_actions_namespace)
    api.add_namespace(blog_locations_namespace)
    api.add_namespace(blog_featuresOfInterest_namespace)
    api.add_namespace(blog_observableProperties_namespace)
    api.add_namespace(blog_unitsOfMeasurement_namespace)
    api.add_namespace(blog_observations_namespace)
    flask_app.register_blueprint(blueprint)

    db.init_app(flask_app)


def main():
    initialize_app(app)
    log.info('>>>>> Starting server at http://{}/api/ <<<<<'.format(app.config['SERVER_NAME']))
    app.run(debug=settings.FLASK_DEBUG)


if __name__ == "__main__":
    main()
