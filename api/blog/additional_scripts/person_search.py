from difflib import SequenceMatcher
import operator

persons = {
    0: "Andreu Fornós",
    1: "Enoc Martínez",
    2: "Marc Martinez",
    3: "Ikram Bghil",
    4: "Javier Cadena",
    5: "Matias Carandell",
    6: "Joaquin Del Rio",
    7: "Daniel Mihai Toma",
    8: "Neus Vidal",
    9: "Carla Atero Delgado"
}


def search(name):
    # names = name.split(' ')
    punctuations = {}
    for id, person in persons.items():
        # person_splited = person.split(' ')
        # similarity = 0
        # for name in names:
        #     for camp in person_splited:
        #         similarity += SequenceMatcher(None, name, camp).ratio()
        # punctuations.update({person: similarity/(len(person_splited))})

        similarity = SequenceMatcher(lambda x: x == ' ', name, person).ratio()
        punctuations.update({person: similarity})
    best_mach = max(punctuations.items(), key=operator.itemgetter(1))[0]
    limit = 0

    if punctuations[best_mach] > 0.48:
        return best_mach
    else:
        return False

