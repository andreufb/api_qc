import werkzeug

werkzeug.cached_property = werkzeug.utils.cached_property
from flask_restplus import fields
from api.restplus import api

pagination = api.model('A page of results', {
    'page': fields.Integer(description='Number of this page of results'),
    'pages': fields.Integer(description='Total number of pages of results'),
    'per_page': fields.Integer(description='Number of items per page of results'),
    'total': fields.Integer(description='Total number of results'),
})

# Authorization
credentials = api.model('Credentials', {
    'email': fields.String(description='email to acces'),
    'password': fields.String(description='password')
})

auth_response = api.model('Response', {
    "token": fields.String(description='Token or error message')
})

# Physical system
identifiers = api.model('Identifiers of the physical system', {
    'definition': fields.Url(),
    'label': fields.String(),
    'value': fields.String()
})

contacts = api.model('Contacts', {
    'definition': fields.Url(),
    'label': fields.String(),
    'email': fields.String(),
    'name': fields.String()
})

observableProperties = api.model('All values related to the physical system', {
    'name': fields.String(),
    'definition': fields.Url(),
    'label': fields.String(),
    'type': fields.String(),
    'uom': fields.Raw()
})

physicalSystem = api.model('Physical system', {
    'idphysicalSystem': fields.String(readOnly=True, description='The unique identifier of a physical system'),
    'identifiers': fields.List(fields.Nested(identifiers)),
    'contacts': fields.List(fields.Nested(contacts)),
    'observableProperties': fields.List(fields.Nested(observableProperties)),
    'attachedTo': fields.String(description='Sensor attached to this device'),
})

input_physicalSystem = api.model('Physical system', {
    'label': fields.String(description='Label of the physical system'),
    'model': fields.String(description='Model of the physical system'),
    'manufacturer': fields.String(description='Manufacturer of the physical system'),
    'serialNumber': fields.String(description='Serial number of the physical system'),
    'uuid': fields.String(description='Uuid of the physical system'),
    'instrumentType': fields.String(description='Type of the physical system'),
    'config': fields.String(description='Configuration of the physical system')
})

physicalSystem_id = api.model('Physical system id', {
    "idphysicalSystem": fields.String(description='Unique identifier of a physical system')
})

# Persons
person = api.model('Person', {
    'idperson': fields.Integer(readOnly=True, description='The unique identifier of a person'),
    'name': fields.String(description='Name of the person'),
    'mail': fields.String(description='Mail of the person'),
    'role': fields.String(description='Role of the person')
})

input_person = api.model('Person', {
    'name': fields.String(description='Name of the person'),
    'mail': fields.String(description='Mail of the person'),
    'role': fields.String(description='Role of the person')
})

search_person = api.model('Search person', {
    "idperson": fields.Integer(description='id of the person'),
    "name": fields.String(description='name of the person')
})

# Projects
participants = api.model('Participants', {
    'idperson': fields.Integer(readOnly=True, description='The unique identifier of a person')
})

project = api.model('Project', {
    'idproject': fields.Integer(readOnly=True, description='The unique identifier of the project'),
    'acronym': fields.String(description='acronym of the project'),
    'fundingEntity': fields.String(description='Funding entity of the project'),
    'grantAgreement': fields.Integer(description='Grant agreement of the project'),
    'initDatetime': fields.DateTime(description='Initial time of the project'),
    'endDatetime': fields.DateTime(description='End date time of the project'),
    'principalInvestigator': fields.Integer(description='The unique identifier of a person'),
    'participants': fields.List(fields.Nested(participants))
})

input_project = api.model('Project', {
    'acronym': fields.String(description='acronym of the project'),
    'fundingEntity': fields.String(description='Funding entity of the project'),
    'grantAgreement': fields.Integer(description='Grant agreement of the project'),
    'initDatetime': fields.DateTime(description='Initial time of the project'),
    'endDatetime': fields.DateTime(description='End date time of the project'),
    'principalInvestigator': fields.Integer(description='The unique identifier of a person'),
    'participants': fields.List(fields.Nested(participants))
})

search_project = api.model('Search project', {
    "idproject": fields.Integer(description='Unique identifier of the project'),
    'acronym': fields.String(description='acronym of the project')
})

# Missions
mission = api.model('Mission', {
    'idmission': fields.Integer(readOnly=True, description='The unique identifier of a mission'),
    'label': fields.String(description='label of a mission'),
    'description': fields.String(description='Description of a mission')
})

input_mission = api.model('Mission', {
    'label': fields.String(description='label of a mission'),
    'description': fields.String(description='Description of a mission')
})

mission_id = api.model('Mission id', {
    "idmission": fields.Integer(description='Unique identifier of a mission')
})

# Actions
action = api.model('Action', {
    'idaction': fields.Integer(readOnly=True, description='The unique identifier of an action'),
    'label': fields.String(description='label of an action'),
    'description': fields.String(description='Description of an action'),
    'uuid': fields.String(description='Uuid of a mission'),
    'time': fields.DateTime(description='Time'),
    'actionTime': fields.DateTime(description='Time of the action'),
    'idphysicalSystem': fields.Integer(description='The unique identifier of the physical system it is attached'),
    'idproject': fields.Integer(description='The unique identifier of a project'),
})

input_action = api.model('Action', {
    'label': fields.String(description='label of an action'),
    'description': fields.String(description='Description of an action'),
    'uuid': fields.String(description='Uuid of a mission'),
    'time': fields.DateTime(description='Time'),
    'actionTime': fields.DateTime(description='Time of the action'),
    'idphysicalSystem': fields.Integer(description='The unique identifier of the physical system it is attached'),
    'idproject': fields.Integer(description='The unique identifier of a project')
})

action_id = api.model('Action id', {
    'idaction': fields.Integer(readOnly=True, description='The unique identifier of an action')
})

# Locations
location = api.model('Location', {
    'idlocation': fields.Integer(readOnly=True, description='The unique identifier of a location'),
    'latitude': fields.Float(description='Latitude position'),
    'longitude': fields.Float(description='Longitude position'),
    'altitude': fields.Float(description='Altitude position'),
    'description': fields.String(description='Description of the location')
})

location_id = api.model('Location id', {
    'idlocation': fields.Integer(readOnly=True, description='The unique identifier of a location')
})

# Features of interest
featureOfInterest = api.model('Feature of interest', {
    'idfeatureOfInterest': fields.Integer(readOnly=True, description='The unique identifier of a feature of interest'),
    'label': fields.String(description='Label of a feature of interest'),
    'link': fields.String(description='URL to the feature of interest'),
    'description': fields.String(description='Description of the feature of interest')
})

featureOfInterest_id = api.model('Feature of interest id', {
    'idfeatureOfInterest': fields.Integer(readOnly=True, description='The unique identifier of a feature of interest')
})

# Observable properties
observableProperty = api.model('Observable property', {
    'idobservableProperty': fields.Integer(readOnly=True,
                                           description='The unique identifier of an observable property'),
    'label': fields.String(description='Label of an observable property'),
    'link': fields.String(description='URL to the an observable property'),
    'description': fields.String(description='Description of the an observable property')
})

observableProperty_id = api.model('Observable property id', {
    'idobservableProperty': fields.Integer(readOnly=True, description='The unique identifier of an observable property')
})

# Units of measurement
unitOfMeasurement = api.model('Unit of measurement', {
    'idunitOfMeasurement': fields.Integer(readOnly=True, description='The unique identifier of a unit of measurement'),
    'code': fields.String(description='Code of the unit of measurement'),
    'link': fields.String(description='URL to the unit of measurement'),
    'description': fields.String(description='Description of the unit of measurement')
})

unitOfMeasurement_id = api.model('Unit of measurement', {
    'idunitOfMeasurement': fields.Integer(readOnly=True, description='The unique identifier of a unit of measurement')
})

# Observations
observation = api.model('Observation', {
    'idobservation':fields.Integer(readOnly=True, description='The unique identifier of an observation'),
    'value':fields.Float(description='Value of the onservation'),
    'quality':fields.Integer(description='Quality value of the observation'),
    'listOfObservations_idlistOfMeasurements': fields.Integer(description='List of observations + measuremetnts')
})

observation_id = api.model('Observation', {
    'idobservation':fields.Integer(readOnly=True, description='The unique identifier of an observation')
})
