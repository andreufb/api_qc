import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
import logging
from flask_restplus import Resource
from flask import request, jsonify
from api.blog.serializers import observation, observation_id
from api.blog.db_handler import Db_Handler as db
from api.restplus import api
from flask_jwt_extended import jwt_required

log = logging.getLogger(__name__)

ns = api.namespace('observation', description='Operations related to OBSEA observations')


@ns.route('/')
class ObservationItems(Resource):
    # @api.marshal_list_with(observation)
    @api.response(204, 'Observations not found.')
    @api.response(200, 'Found observations.')
    # @jwt_required
    def get(self):
        """
        Returns all OBSEA observations.
        """
        try:
            return jsonify(db().get_observation())
        except:
            return "Not found observations.", 204

    @api.marshal_with(observation)
    @api.expect(observation_id)
    @api.response(400, 'Bad request.')
    @api.response(204, 'Observation not found.')
    @api.response(200, 'Observation found.')
    # @jwt_required
    def post(self):
        """
        Returns the description of an observation.
        """
        try:
            observation = db().get_observation(request.json["idobservation"])
        except:
            return None, 400

        if not observation:
            return "Could not find the observation, revise the id value.", 204
        else:
            return observation, 200


@ns.route('/<int:idobservation>')
@api.response(204, 'Observation not found.')
@api.response(200, 'Found observation.')
class ObservationItemsGet(Resource):
    @api.marshal_with(observation)
    # @jwt_required
    def get(self, idobservation):
        """
        Returns the description of an observation.
        """
        observation = db().get_observation(idobservation)
        if not observation:
            return "Could not find the observation, revise the id value.", 204
        else:
            return observation, 200
