import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
import logging
from flask_restplus import Resource
from flask import request, jsonify
from api.blog.serializers import person, input_person, search_person
from api.blog.db_handler import Db_Handler as db
from api.blog.additional_scripts.person_search import search
from api.restplus import api
from flask_jwt_extended import jwt_required

log = logging.getLogger(__name__)

ns = api.namespace('person', description='Operations related to OBSEA personal')

@ns.route('/')
class PersonItems(Resource):

    # @api.marshal_list_with(person)
    @api.response(204, 'Persons not found.')
    @api.response(200, 'Found persons.')
    # @jwt_required
    def get(self):
        """
        Returns all OBSEA persons.
        """
        try:
            return jsonify(db().get_person())
        except:
            return "Not found persons.", 204


    @api.marshal_with(person)
    @api.expect(search_person)
    @api.response(400, 'Bad request.')
    @api.response(204, 'Person not found.')
    @api.response(210, 'Best mach for the person name')
    @api.response(200, 'Found person.')
    # @jwt_required
    def post(self):
        """
        Returns the description of a person.
        """
        try:
            idperson = request.json["idperson"]
            name = request.json["name"]
        except:
            return None, 400

        person = db().get_person(idperson, name)
        if not person:
            best_mach = search(name=name)
            if best_mach:
                person = db().get_person(idperson, best_mach)
                return person, 210
            else:
                return 'Person or similar person not found', 204

        else:
            return person, 200


    @api.expect(input_person)
    @api.response(405, 'Could not append new person.')
    @api.response(201, 'Person added.')
    # @jwt_required
    def put(self):
        """
        Add a new person.
        """
        person_inserted = db().insert_person(request.json)
        if person_inserted:
            return ("Added person:", request.json), 201
        else:
            return "The person could not be added, revise that all required camps are dispatched.", 405



@ns.route('/<int:idperson>')
@ns.route('/<name>')
@api.response(204, 'Person not found.')
@api.response(200, 'Found person.')
class PersonItemsGet(Resource):
    @api.marshal_with(person)
    # @jwt_required
    def get(self, idperson = None, name = None):
        """
        Returns the description of a person.
        """
        person = db().get_person(idperson, name)
        if not person:
            return "Could not find the person, revise the id value.", 204
        else:
            return person, 200


