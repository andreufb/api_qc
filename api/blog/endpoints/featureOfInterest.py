import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
import logging
from flask_restplus import Resource
from flask import request, jsonify
from api.blog.serializers import featureOfInterest, featureOfInterest_id
from api.blog.db_handler import Db_Handler as db
from api.restplus import api
from flask_jwt_extended import jwt_required

log = logging.getLogger(__name__)

ns = api.namespace('feature-of-interest', description='Operations related to the OBSEA features of interest')


@ns.route('/')
class FeatureOfInterestItems(Resource):
    # @api.marshal_list_with(featureOfInterest)
    @api.response(204, 'Features of interest not found.')
    @api.response(200, 'Found features of interest.')
    # @jwt_required
    def get(self):
        """
        Returns all OBSEA features of interest.
        """
        try:
            return jsonify(db().get_featureOfInterest())
        except:
            return "Not found features of interest.", 204

    @api.marshal_with(featureOfInterest)
    @api.expect(featureOfInterest_id)
    @api.response(400, 'Bad request.')
    @api.response(204, 'Feature of interest not found.')
    @api.response(200, 'Feature of interest found.')
    # @jwt_required
    def post(self):
        """
        Returns the description of a feature of interest.
        """
        try:
            feature_of_interest = db().get_featureOfInterest(request.json["idfeatureOfInterest"])
        except:
            return None, 400

        if not feature_of_interest:
            return "Could not find the feature of interest, revise the id value.", 204
        else:
            return feature_of_interest, 200


@ns.route('/<int:idfeatureOfInterest>')
@api.response(204, 'Location not found.')
@api.response(200, 'Found location.')
class FeatureOfInterestItemsGet(Resource):
    @api.marshal_with(featureOfInterest)
    # @jwt_required
    def get(self, idfeatureOfInterest):
        """
        Returns the description of a feature of interest.
        """
        feature_of_interest = db().get_featureOfInterest(idfeatureOfInterest)
        if not feature_of_interest:
            return "Could not find the feature of interest, revise the id value.", 204
        else:
            return feature_of_interest, 200