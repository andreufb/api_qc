import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
import logging
from flask_restplus import Resource
from flask import request, jsonify
from api.blog.serializers import location, location_id
from api.blog.db_handler import Db_Handler as db
from api.restplus import api
from flask_jwt_extended import jwt_required

log = logging.getLogger(__name__)

ns = api.namespace('location', description='Operations related to SARTI locations')


@ns.route('/')
class LocactionItems(Resource):
    # @api.marshal_list_with(location)
    @api.response(204, 'Locations not found.')
    @api.response(200, 'Found locations.')
    # @jwt_required
    def get(self):
        """
        Returns all SARTI locations.
        """
        try:
            return jsonify(db().get_location())
        except:
            return "Not found locations.", 204


    @api.marshal_with(location)
    @api.expect(location_id)
    @api.response(400, 'Bad request.')
    @api.response(204, 'Location not found.')
    @api.response(200, 'Location found.')
    # @jwt_required
    def post(self):
        """
        Returns the description of a location.
        """
        try:
            location = db().get_location(request.json["idlocation"])
        except:
            return None, 400

        if not location:
            return "Could not find the location, revise the id value.", 204
        else:
            return location, 200



@ns.route('/<int:idlocation>')
@api.response(204, 'Location not found.')
@api.response(200, 'Found location.')
class LocationItemsGet(Resource):
    @api.marshal_with(location)
    # @jwt_required
    def get(self, idlocation):
        """
        Returns the description of a location.
        """
        location = db().get_location(idlocation)
        if not location:
            return "Could not find the location, revise the id value.", 204
        else:
            return location, 200
