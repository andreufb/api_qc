import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
import logging
from flask_restplus import Resource
from flask import request, jsonify
from api.blog.serializers import project, input_project, search_project
from api.blog.db_handler import Db_Handler as db
from api.restplus import api
from flask_jwt_extended import jwt_required


log = logging.getLogger(__name__)

ns = api.namespace('project', description='Operations related to OBSEA projects')

@ns.route('/')
class ProjectItems(Resource):
    # @api.marshal_list_with(project)
    @api.response(204, 'Project not found.')
    @api.response(200, 'Found projects.')
    # @jwt_required
    def get(self):
        """
        Returns all OBSEA projects.
        """
        try:
            return jsonify(db().get_project())
        except:
            return "Not found projects.", 204


    @api.marshal_with(project)
    @api.expect(search_project)
    @api.response(400, 'Bad request.')
    @api.response(204, 'Project not found.')
    @api.response(200, 'Project found.')
    # @jwt_required
    def post(self):
        """
        Returns the description of a project.
        """
        try:
            idproject = request.json['idproject']
            acronym = request.json['acronym']
        except:
            return None, 400
        project = db().get_project(idproject, acronym)
        if not project:
            return "Could not find the project, revise the id value.", 204
        else:
            return project, 200


    @api.expect(input_project)
    @api.response(405, 'Could not add new project.')
    @api.response(201, 'Project configured.')
    # @jwt_required
    def put(self):
        """
        Add a new project.
        """
        project_inserted = db().insert_project(request.json)
        if project_inserted:
            return ("Configured project:", request.json), 201
        else:
            return "The poject could not be added, revise that all required camps are dispatched.", 405


@ns.route('/<int:idproject>')
@ns.route('/<acronym>')
@api.response(204, 'Project not found.')
@api.response(200, 'Found project.')
class ProjectItemsGet(Resource):
    @api.marshal_with(project)
    # @jwt_required
    def get(self, idproject = None, acronym = None):
        """
        Returns the description of a project.
        """
        project = db().get_project(idproject, acronym)
        if not project:
            return "Could not find the project, revise the id value.", 204
        else:
            return project, 200