import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
import logging
from flask_restplus import Resource
from flask import request, jsonify
from api.blog.serializers import action, input_action, action_id
from api.blog.db_handler import Db_Handler as db
from api.restplus import api
from flask_jwt_extended import jwt_required

log = logging.getLogger(__name__)

ns = api.namespace('action', description='Operations related to OBSEA actions')


@ns.route('/')
class ActionItems(Resource):
    # @api.marshal_list_with(action)
    @api.response(204, 'Actions not found.')
    @api.response(200, 'Found actions.')
    # @jwt_required
    def get(self):
        """
        Returns all OBSEA actions.
        """
        try:
            return jsonify(db().get_action())
        except:
            return "Not found actions.", 204


    @api.marshal_with(action)
    @api.expect(action_id)
    @api.response(400, 'Bad request.')
    @api.response(204, 'Action not found.')
    @api.response(200, 'Action found.')
    # @jwt_required
    def post(self):
        """
        Returns the description of an action.
        """
        try:
            action = db().get_action(request.json["idaction"])
        except:
            return None, 400

        if not action:
            return "Could not find the action, revise the id value.", 204
        else:
            return action, 200


    @api.expect(input_action)
    @api.response(405, 'Could not configure the new action.')
    @api.response(201, 'Action configured.')
    # @jwt_required
    def put(self):
        """
        Add new action.
        """
        action_inserted = db().insert_action(request.json)
        if action_inserted:
            return ("Configured action:", request.json), 201
        else:
            return "The action could not be added, revise that all required camps are dispatched.", 405



@ns.route('/<int:idaction>')
@api.response(204, 'Action not found.')
@api.response(200, 'Found action.')
class ActionItemsGet(Resource):
    @api.marshal_with(action)
    # @jwt_required
    def get(self, idaction):
        """
        Returns the description of an action.
        """
        action = db().get_action(idaction)

        if not action:
            return "Could not find the action, revise the id value.", 204
        else:
            return action, 200
