import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
import logging
from flask_restplus import Resource
from flask import request, jsonify
from api.blog.serializers import observableProperty, observableProperty_id
from api.blog.db_handler import Db_Handler as db
from api.restplus import api
from flask_jwt_extended import jwt_required

log = logging.getLogger(__name__)

ns = api.namespace('observable-property', description='Operations related to the OBSEA observable properties')


@ns.route('/')
class ObservablePropertyItems(Resource):
    # @api.marshal_list_with(observableProperty)
    @api.response(204, 'Observable properties not found.')
    @api.response(200, 'Found observable properties.')
    # @jwt_required
    def get(self):
        """
        Returns all OBSEA observable properties.
        """
        try:
            return jsonify(db().get_observableProperty())
        except:
            return "Not found observable properties.", 204

    @api.marshal_with(observableProperty)
    @api.expect(observableProperty_id)
    @api.response(400, 'Bad request.')
    @api.response(204, 'Observable property not found.')
    @api.response(200, 'Observable property found.')
    # @jwt_required
    def post(self):
        """
        Returns the description of an observable property.
        """
        try:
            observable_property = db().get_observableProperty(request.json["idobservableProperty"])
        except:
            return None, 400

        if not observable_property:
            return "Could not find the feature of interest, revise the id value.", 204
        else:
            return observable_property, 200


@ns.route('/<int:idobservableProperty>')
@api.response(204, 'Observable property not found.')
@api.response(200, 'Observable property found.')
class ObservablePropertyItemsGet(Resource):
    @api.marshal_with(observableProperty)
    # @jwt_required
    def get(self, idobservableProperty):
        """
        Returns the description of an observable property.
        """
        observable_property = db().get_featureOfInterest(idobservableProperty)
        if not observable_property:
            return "Could not find the observable property, revise the id value.", 204
        else:
            return observable_property, 200
        