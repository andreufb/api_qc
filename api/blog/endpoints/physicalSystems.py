import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
import logging
from flask_restplus import Resource
from flask import request, jsonify
from api.blog.serializers import physicalSystem, input_physicalSystem, physicalSystem_id
from api.blog.db_handler import Db_Handler as db
from api.restplus import api
from flask_jwt_extended import jwt_required

log = logging.getLogger(__name__)

ns = api.namespace('physical-system', description='Operations related to OBSEA physical system')


@ns.route('/')
class PhysicalSystemItems(Resource):

    @api.response(204, 'Physical systems not found.')
    @api.response(200, 'Found physical systems.')
    # @jwt_required
    def get(self):
        """
        Returns all OBSEA physical systems.
        """
        try:
            return jsonify(db().get_physicalSystem())
        except:
            return "Not found physical systems.", 204


    @api.marshal_with(physicalSystem)
    @api.expect(physicalSystem_id)
    @api.response(400, 'Bad request.')
    @api.response(204, 'Physical system not found.')
    @api.response(200, 'Found physical system.')
    # @jwt_required
    def post(self):
        """
        Returns the description of a physical system.
        """
        try:
            physicalsystem = db().get_physicalSystem(request.json["idphysicalSystem"])
        except:
            return None, 400
        if not physicalsystem:
            return "Could not find the physical system, revise the id value.", 204
        else:
            return physicalsystem, 200


    @api.expect(input_physicalSystem)
    @api.response(405, 'Could not append new physical system.')
    @api.response(201, 'Physical system configured.')
    # @jwt_required
    def put(self):
        """
        Configure a new physical system.
        """
        physicalSystem_inserted = db().insert_physicalSystem(request.json)
        if physicalSystem_inserted:
            return ("Configured physical system:", request.json), 201
        else:
            return "The physical system could not be configured, revise that all required camps are dispatched.", 405



@ns.route('/<idphysicalSystem>')
@api.response(204, 'Physical system not found.')
class PhysicalSystemItemsGet(Resource):
    # @api.marshal_with(physicalSystem)
    @api.response(200, 'Found physical system.')
    # @jwt_required
    def get(self, idphysicalSystem):
        """
        Returns the description of a physical system.
        """
        physicalSystem = db().get_physicalSystem(idphysicalSystem)
        if not physicalSystem:
            return "Could not find the physical system, revise the id value.", 204
        else:
            return physicalSystem, 200