import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
import logging
from api.restplus import api
from api.blog.serializers import credentials, auth_response
from api.blog.db_handler import Db_Handler as db
from flask_jwt_extended import create_access_token
import datetime
from flask import request
from flask_restplus import Resource

log = logging.getLogger(__name__)

ns = api.namespace('auth', description='Authorization')

@ns.route('/')
@api.response(400, 'Could not access')
class AccessApi(Resource):
    @api.expect(credentials)
    @api.marshal_with(auth_response)
    @api.response(201, 'Access granted.')
    @api.response(400, 'Bad request.')
    def post(self):
        """
        Authenticates users to grant access to the api.
        """
        try:
            data = request.json
            registered_user = db().registered_user(data)
        except:
            return None, 400

        if not registered_user:
            return "user not authorized", 400

        expires = datetime.timedelta(days=0.041667)
        access_token = create_access_token(identity=data['email'], expires_delta=expires)
        return {'token': access_token}, 201
