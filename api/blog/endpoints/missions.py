import werkzeug

werkzeug.cached_property = werkzeug.utils.cached_property
import logging
from flask_restplus import Resource
from flask import request, jsonify
from api.blog.serializers import mission, input_mission, mission_id
from api.blog.db_handler import Db_Handler as db
from api.restplus import api
from flask_jwt_extended import jwt_required

log = logging.getLogger(__name__)

ns = api.namespace('mission', description='Operations related to OBSEA missions')


@ns.route('/')
class MissionItems(Resource):
    # @api.marshal_list_with(missions)
    # @jwt_required
    @api.response(204, 'Mission not found.')
    @api.response(200, 'Found missions.')
    def get(self):
        """
        Returns all OBSEA missions.
        """
        try:
            return jsonify(db().get_mission())
        except:
            return "Not found missions.", 204


    @api.marshal_with(mission)
    @api.expect(mission_id)
    @api.response(400, 'Bad request.')
    @api.response(204, 'Missions not found.')
    @api.response(200, 'Mission found.')
    # @jwt_required
    def post(self):
        """
        Returns the description of a mission.
        """
        try:
            mission = db().get_mission(request.json["idmission"])
        except:
            return None, 400
        if not mission:
            return "Could not find the mission, revise the id value.", 204
        else:
            return mission, 200

    @api.expect(input_mission)
    @api.response(405, 'Could not configure the new mission.')
    @api.response(201, 'Mission configured.')
    # @jwt_required
    def put(self):
        """
        Add new mission.
        """
        mission_inserted = db().insert_mission(request.json)
        if mission_inserted:
            return ("Configured mission:", request.json), 201
        else:
            return "The mission could not be added, revise that all required camps are dispatched.", 405


@ns.route('/<int:idmission>')
@api.response(204, 'Mission not found.')
@api.response(200, 'Found mission.')
class MissionItemsGet(Resource):
    @api.marshal_with(mission)
    # @jwt_required
    def get(self, idmission):
        """
        Returns the description of a mission.
        """
        mission = db().get_mission(idmission)
        if not mission:
            return "Could not find the mission, revise the id value.", 204
        else:
            return mission, 200