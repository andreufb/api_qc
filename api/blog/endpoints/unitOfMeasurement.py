import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
import logging
from flask_restplus import Resource
from flask import request, jsonify
from api.blog.serializers import unitOfMeasurement, unitOfMeasurement_id
from api.blog.db_handler import Db_Handler as db
from api.restplus import api
from flask_jwt_extended import jwt_required

log = logging.getLogger(__name__)

ns = api.namespace('unit-of-measurement', description='Operations related to the OBSEA units of measurement')


@ns.route('/')
class UnitOfMeasurementItems(Resource):
    # @api.marshal_list_with(unitOfMeasurement)
    @api.response(204, 'Unit of measurement not found.')
    @api.response(200, 'Found unit of measurement.')
    # @jwt_required
    def get(self):
        """
        Returns all OBSEA units of measurement.
        """
        try:
            return jsonify(db().get_unitOfMeasurement())
        except:
            return "Not found unit of measurement.", 204

    @api.marshal_with(unitOfMeasurement)
    @api.expect(unitOfMeasurement_id)
    @api.response(400, 'Bad request.')
    @api.response(204, 'Unit of measurement not found.')
    @api.response(200, 'Unit of measurement found.')
    # @jwt_required
    def post(self):
        """
        Returns the description of a unit of measurement.
        """
        try:
            unit_of_measurement = db().get_unitOfMeasurement(request.json["idunitOfMeasurement"])
        except:
            return None, 400

        if not unit_of_measurement:
            return "Could not find the unit of measurement, revise the id value.", 204
        else:
            return unit_of_measurement, 200


@ns.route('/<int:idunitOfMeasurement>')
@api.response(204, 'Unit of measurement not found.')
@api.response(200, 'Found unit of measurement.')
class UnitOfMeasurementItemsGet(Resource):
    @api.marshal_with(unitOfMeasurement)
    # @jwt_required
    def get(self, idunitOfMeasurement):
        """
        Returns the description of a unit of measurement.
        """
        unit_of_measurement = db().get_unitOfMeasurement(idunitOfMeasurement)
        if not unit_of_measurement:
            return "Could not find the unit of measurement, revise the id value.", 204
        else:
            return unit_of_measurement, 200