from api.database import db
import datetime
# from api.database.models import PhysicalSystems, Persons, Projects, Missions, Actions
from api.database.models import Device


class Db_Handler:

    # Auth
    def registered_user(self, input_data):
        users = {"andreufb97@gmail.com": "1234", "enoc.martinez@upc.edu": "1234",
                 "marc.martinez@cdsarti.org": "1234", "ikram.bghil@cdsarti.org": "1234"}

        email = input_data['email']
        password = input_data['password']

        for user, credential in users.items():
            if user == email and credential == password:
                return True
        return False

    # Physical systems
    def insert_physicalSystem(self, input_data):
        try:
            label = input_data['label']
            model = input_data['model']
            manufacturer = input_data['manufacturer']
            serialNumber = input_data['serialNumber']
            uuid = input_data['uuid']
            instrumentType = input_data['instrumentType']
            config = input_data['config']
            return True

        except:
            return False

    def get_physicalSystem(self, physicalSystem_id=None):
        physicalSystems = [

            {
                "idphysicalSystem": "SBE37:14988",
                "identifiers": [
                    {
                        "definition": "http://vocab.nerc.ac.uk/collection/W07/current/IDEN0002/",
                        "label": "long name",
                        "value": "Sea-Bird Electronics 37"
                    },
                    {
                        "definition": "http://vocab.nerc.ac.uk/collection/W07/current/IDEN0006/",
                        "label": "short name",
                        "value": "SBE37"
                    },
                    {
                        "definition": "http://vocab.nerc.ac.uk/collection/W07/current/IDEN0005/",
                        "label": "serial number",
                        "value": "14987"
                    }],
                "contacts": [
                    {
                        "definition": "http://vocab.nerc.ac.uk/collection/W08/current/CONT0003/",
                        "label": "operator",
                        "email": "enoc.martinez@upc.edu",
                        "name": "Enoc Martínez"
                    },
                    {
                        "definition": "http://vocab.nerc.ac.uk/collection/W08/current/CONT0002/",
                        "label": "principal investigator",
                        "email": "joaquin.del.rio@upc.edu",
                        "name": "Joaquín del Río"
                    }],
                "observableProperties": [
                    {
                        "name": "sea_water_temperature",
                        "definition": "http://vocab.nerc.ac.uk/standard_name/sea_water_temperature",
                        "label": "sea water temperature",
                        "type": "Quantity",
                        "uom": {
                            "code": "degC",
                            "definition": "http://vocab.nerc.ac.uk/collection/P06/current/UPAA"
                        }
                    },
                    {
                        "name": "sea_water_salinity",
                        "definition": "http://vocab.nerc.ac.uk/standard_name/sea_water_salinity",
                        "label": "sea water temperature",
                        "type": "Quantity",
                        "uom": {
                            "code": "ppt",
                            "definition": "http://vocab.nerc.ac.uk/collection/P06/current/UPPT"
                        }
                    },
                    {
                        "name": "depth",
                        "definition": "http://vocab.nerc.ac.uk/standard_name/depth",
                        "label": "depth",
                        "type": "Quantity",
                        "uom": {
                            "code": "m",
                            "definition": "http://vocab.nerc.ac.uk/collection/P06/current/ULAA"
                        }
                    }],
                "attachedTo": "OBSEA"
            },

            {
                "idphysicalSystem": "SBE37:14987",
                "identifiers": [
                    {
                        "definition": "http://vocab.nerc.ac.uk/collection/W07/current/IDEN0002/",
                        "label": "long name",
                        "value": "Sea-Bird Electronics 37"
                    },
                    {
                        "definition": "http://vocab.nerc.ac.uk/collection/W07/current/IDEN0006/",
                        "label": "short name",
                        "value": "SBE37"
                    },
                    {
                        "definition": "http://vocab.nerc.ac.uk/collection/W07/current/IDEN0005/",
                        "label": "serial number",
                        "value": "14987"
                    }],
                "contacts": [
                    {
                        "definition": "http://vocab.nerc.ac.uk/collection/W08/current/CONT0003/",
                        "label": "operator",
                        "email": "enoc.martinez@upc.edu",
                        "name": "Enoc Martínez"
                    },
                    {
                        "definition": "http://vocab.nerc.ac.uk/collection/W08/current/CONT0002/",
                        "label": "principal investigator",
                        "email": "joaquin.del.rio@upc.edu",
                        "name": "Joaquín del Río"
                    }],
                "observableProperties": [
                    {
                        "name": "sea_water_temperature",
                        "definition": "http://vocab.nerc.ac.uk/standard_name/sea_water_temperature",
                        "label": "sea water temperature",
                        "type": "Quantity",
                        "uom": {
                            "code": "degC",
                            "definition": "http://vocab.nerc.ac.uk/collection/P06/current/UPAA"
                        }
                    },
                    {
                        "name": "sea_water_salinity",
                        "definition": "http://vocab.nerc.ac.uk/standard_name/sea_water_salinity",
                        "label": "sea water temperature",
                        "type": "Quantity",
                        "uom": {
                            "code": "ppt",
                            "definition": "http://vocab.nerc.ac.uk/collection/P06/current/UPPT"
                        }
                    },
                    {
                        "name": "depth",
                        "definition": "http://vocab.nerc.ac.uk/standard_name/depth",
                        "label": "depth",
                        "type": "Quantity",
                        "uom": {
                            "code": "m",
                            "definition": "http://vocab.nerc.ac.uk/collection/P06/current/ULAA"
                        }
                    }],
                "attachedTo": "OBSEA"
            }]

        if physicalSystem_id is None:
            return physicalSystems

        for physicalSystem in physicalSystems:
            if physicalSystem["idphysicalSystem"] == physicalSystem_id:
                return physicalSystem
        return False

    # Persons
    def insert_person(self, input_data):
        try:
            name = input_data['name']
            mail = input_data['mail']
            role = input_data['role']
            return True
        except:
            return False

    def get_person(self, person_id=None, name=None):
        persons = [
            {
                "idperson": 1,
                "name": "Andreu Fornós",
                "mail": "andreufb97@gmail.com",
                "role": "informatic"
            },
            {
                "idperson": 2,
                "name": "Enoc Martínez",
                "mail": "enoc.martinez@upc.edu",
                "role": "informatic"
            },
            {
                "idperson": 3,
                "name": "Marc Martinez",
                "mail": "marc.martinez@cdsarti.org",
                "role": "informatic"
            },
            {
                "idperson": 4,
                "name": "Ikram Bghil",
                "mail": "ikram.bghil@cdsarti.org",
                "role": "informatica"
            },
            {
                "idperson": 5,
                "name": "Javier Cadena",
                "mail": "javier.cadena@upc.edu ",
                "role": "informatic"
            },
            {
                "idperson": 6,
                "name": "Matias Carandell",
                "mail": "ematias.carandell@upc.edu",
                "role": "informatic"
            },
            {
                "idperson": 7,
                "name": "Daniel Mihai Toma",
                "mail": "daniel.mihai.toma@epse.upc.edu",
                "role": "informatic"
            },
            {
                "idperson": 8,
                "name": "Neus Vidal",
                "mail": "neus.vidal@upc.edu ",
                "role": "informatica"
            },
            {
                "idperson": 9,
                "name": "Carla Atero Delgado",
                "mail": "carla.atero@upc.edu ",
                "role": "informatica"
            },
            {
                "idperson": 10,
                "name": "Joaquin Del Rio",
                "mail": "joaquin.del.rio@upc.edu",
                "role": "informatic"
            }
        ]

        if person_id == name is None:
            return persons

        for person in persons:
            if person['name'] == name or person['idperson'] == person_id:
                return person
        return False

    # Projects
    def insert_project(self, input_data):
        try:
            acronym = input_data['acronym']
            fundingEntity = input_data['fundingEntity']
            grantAgreement = input_data['grantAgreement']
            initDatetime = input_data['initDatetime']
            endDatetime = input_data['endDatetime']
            principalInvestigator = input_data['principalInvestigator']
            participants = input_data['participants']
            return True
        except:
            return False

    def get_project(self, project_id=None, acronym=None):
        projects = [
            {
                "idproject": 1,
                "acronym": "YEYE",
                "fundingEntity": "SomeRichPerson",
                "grantAgreement": 123123,
                "initDatetime": datetime.date(2019, 5, 20),
                "endDatetime": datetime.date(2020, 4, 13),
                "principalInvestigator": 2,
                "participants": [
                    {
                        "idperson": 1
                    },
                    {
                        "idperson": 3
                    }
                ]
            },
            {
                "idproject": 2,
                "acronym": "NDB",
                "fundingEntity": "UPC",
                "grantAgreement": 30000,
                "initDatetime": datetime.date(2020, 2, 14),
                "endDatetime": datetime.date(2050, 4, 20),
                "principalInvestigator": 1,
                "participants": [
                    {
                        "idperson": 1
                    },
                    {
                        "idperson": 3
                    }
                ]
            },
            {
                "idproject": 3,
                "acronym": "p",
                "fundingEntity": "Proba",
                "grantAgreement": 807242,
                "initDatetime": datetime.date(2019, 11, 23),
                "endDatetime": datetime.date(2020, 12, 20),
                "principalInvestigator": 3,
                "participants": [
                    {
                        "idperson": 1
                    },
                    {
                        "idperson": 2
                    }
                ]
            }
        ]

        if project_id == acronym is None:
            return projects

        for project in projects:
            if project['acronym'] == acronym or project['idproject'] == project_id:
                return project
        return False

    # Missions
    def insert_mission(self, input_data):
        try:
            label = input_data['label']
            description = input_data['description']
            return True
        except:
            return False

    def get_mission(self, mission_id=None):
        missions = [
            {
                "idmission": 1,
                "label": "mission1",
                "description": "primera missio que te guardada la API!"
            },
            {
                "idmission": 2,
                "label": "proba_missio",
                "description": "proba per veure si funciona esta cosa"
            },
            {
                "idmission": 3,
                "label": "Develop new DB",
                "description": "Develop the new data-base for the data acquired by the sensors atached to the OBSEA"
            }
        ]

        if mission_id is None:
            return missions

        for mission in missions:
            if mission["idmission"] == mission_id:
                return mission
        return False

    # Action
    def insert_action(self, input_data):
        try:
            label = input_data['label']
            description = input_data['description']
            uuid = input_data['uuid']
            time = input_data['time']
            actionTime = input_data['actionTime']
            idphysicalSystem = input_data['idphysicalSystem']
            idproject = input_data['idproject']
            return True
        except:
            return False

    def get_action(self, action_id=None):
        actions = [
            {
                'idaction': 0,
                'label': 'Primera action',
                'description': 'yeye',
                'uuid': 'b',
                'time': datetime.date(2019, 5, 20),
                'actionTime': datetime.date(2019, 5, 20),
                'idphysicalSystem': 1,
                'idproject': 2
            },
            {
                'idaction': 1,
                'label': 'dad',
                'description': 'a',
                'uuid': 'avva3b',
                'time': datetime.date(2019, 5, 20),
                'actionTime': datetime.date(2019, 5, 20),
                'idphysicalSystem': 1,
                'idproject': 2
            },
            {
                'idaction': 2,
                'label': 'dad',
                'description': 'a',
                'uuid': 'b',
                'time': datetime.date(2019, 3, 20),
                'actionTime': datetime.date(2020, 5, 20),
                'idphysicalSystem': 3,
                'idproject': 1
            }
        ]

        if action_id is None:
            return actions

        for action in actions:
            if action['idaction'] == action_id:
                return action
        return False

    # Location
    def insert_location(self, input_data):
        try:
            latitude = input_data['latitude']
            longitude = input_data['longitude']
            altitude = input_data['altitude']
            description = input_data['description']
            return True
        except:
            return False

    def get_location(self, location_id=None):
        locations = [
            {
                'idlocation': 0,
                'latitude': 42.232,
                'longitude': 1.112,
                'altitude': -19.8,
                'description': 'Obsea position'
            },
            {
                'idlocation': 1,
                'latitude': 43.232,
                'longitude': 22.112,
                'altitude': 23.223,
                'description': 'Somethung'
            },
            {
                'idlocation': 2,
                'latitude': 56.232,
                'longitude': 23.112,
                'altitude': 8102.212,
                'description': 'Everest'
            },
            {
                'idlocation': 3,
                'latitude': -12.232,
                'longitude': -90.112,
                'altitude': 2.1,
                'description': 'Who knows?'
            }
        ]

        if location_id is None:
            return locations

        for location in locations:
            if location['idlocation'] == location_id:
                return location
        return False

    # Feature of interest
    def insert_featureOfInterest(self, input_data):
        try:
            label = input_data['label']
            link = input_data['link']
            description = input_data['description']
            return True
        except:
            return False

    def get_featureOfInterest(self, featureOfInterest_id=None):
        featuresOfInterest = [
            {
                'idfeatureOfInterest': 0,
                'label': 'Feature of interest 1',
                'link': "http://vocab.nerc.ac.uk/collection/W08/current/CONT0003/",
                'description': 'The first feature of interes of the API'
            },
            {
                'idfeatureOfInterest': 1,
                'label': 'Bears',
                'link': "http://vocab.nerc.ac.uk/collection/W08/current/CONT0003/",
                'description': 'Bears have some interesting things...'
            },
            {
                'idfeatureOfInterest': 2,
                'label': 'yeyee',
                'link': "http://vocab.nerc.ac.uk/collection/W08/current/CONT0003/",
                'description': 'tetee'
            },
            {
                'idfeatureOfInterest': 3,
                'label': 'HEYyeee',
                'link': "http://vocab.nerc.ac.uk/collection/W08/current/CONT0003/",
                'description': 'something!'
            }
        ]

        if featureOfInterest_id is None:
            return featuresOfInterest

        for featureOfInterest in featuresOfInterest:
            if featureOfInterest['idfeatureOfInterest'] == featureOfInterest_id:
                return featureOfInterest
        return False

    # Observable properties
    def insert_observableProperty(self, input_data):
        try:
            label = input_data['label']
            link = input_data['link']
            description = input_data['description']
            return True
        except:
            return False

    def get_observableProperty(self, observableProperty_id=None):
        observableProperties = [
            {
                'idobservableProperty': 0,
                'label': 'proba a',
                'link': 'http://wwww.google.es',
                'description': 'YEYE'
            },
            {
                'idobservableProperty': 1,
                'label': 'label lael ll',
                'link': 'http://wwww.google.es',
                'description': 'TUTURURU'
            },
            {
                'idobservableProperty': 2,
                'label': 'yupyup',
                'link': 'http://wwww.google.es',
                'description': 'works?'
            },
            {
                'idobservableProperty': 3,
                'label': 'RT',
                'link': 'http://wwww.google.es',
                'description': 'YHLQMDLG'
            },
        ]

        if observableProperty_id is None:
            return observableProperties

        for observableProperty in observableProperties:
            if observableProperty['idobservableProperty'] == observableProperty_id:
                return observableProperty
        return False

    # Units of measurement
    def insert_unitOfMeasurement(self, input_data):
        try:
            code = input_data['code']
            link = input_data['link']
            description = input_data['description']
            return True
        except:
            return False

    def get_unitOfMeasurement(self, unitOfMeasurement_id=None):
        unitsOfMeasurement = [
            {
                'idunitOfMeasurement': 0,
                'code': 'CODE1',
                'link': 'http://wwww.google.es',
                'description': 'Temperature'
            },
            {
                'idunitOfMeasurement': 1,
                'code': 'BBBYBE',
                'link': 'http://wwww.google.es',
                'description': 'Salinity'
            },
            {
                'idunitOfMeasurement': 2,
                'code': 'YEYETU',
                'link': 'http://wwww.google.es',
                'description': 'Depth'
            },
            {
                'idunitOfMeasurement': 3,
                'code': 'CON:CODI:1231',
                'link': 'http://wwww.google.es',
                'description': 'Conductivity'
            }
        ]

        if unitOfMeasurement_id is None:
            return unitsOfMeasurement

        for unitOfMeasurement in unitsOfMeasurement:
            if unitOfMeasurement['idunitOfMeasurement'] == unitOfMeasurement_id:
                return unitOfMeasurement
        return False


    # Observations
    def insert_observation(self, input_data):
        try:
            value = input_data['value']
            quality = input_data['quality']
            listOfObservations_idlistOfMeasurements = input_data['listOfObservations_idlistOfMeasurements']
            return True
        except:
            return False

    def get_observation(self, observation_id = None):
        observations = [
            {
                'idobservation': 0,
                'value': 1.1,
                'quality': 2,
                'listOfObservations_idlistOfMeasurements': 0,
            },
            {
                'idobservation': 1,
                'value': 1.1,
                'quality': 4,
                'listOfObservations_idlistOfMeasurements': 0,
            },
            {
                'idobservation': 2,
                'value': 41.112,
                'quality': 1,
                'listOfObservations_idlistOfMeasurements': 0,
            },
            {
                'idobservation': 3,
                'value': 12.1,
                'quality': 9,
                'listOfObservations_idlistOfMeasurements': 0,
            },
        ]

        if observation_id is None:
            return observations

        for observation in observations:
            if observation['idobservation'] == observation_id:
                return observation
        return False



    # Devices - Example of how take info from db
    # def create_device(data):
    #     name = data.get('name')
    #     definition = data.get('definition')
    #     device_id = data.get('id')
    #     device = Device(name, definition)
    #
    #     if device_id:
    #         device.id = device_id
    #
    #     db.session.add(device)
    #     db.session.commit()

    # def update_device(device_id, data):
    #     device = Device.query.filter(Device.id == device_id).one()
    #     device.name = data.get('name')
    #     device.definition = data.get('definition')
    #     db.session.add(device)
    #     db.session.commit()

    # def delete_device(device_id):
    #     device = Device.query.filter(Device.id == device_id).one()
    #     db.session.delete(device)
    #     db.session.commit()
