import requests
import random

URL = 'http://147.83.159.160:5000/api/'

# Jsons to post good
p_auth = {
    "email": "andreufb97@gmail.com",
    "password": "1234"
}
p_physicalSystem = {
    "idphysicalSystem": "SBE37:14988"
}
p_person_name = {
    "idperson": 123123,
    "name": "Andreu Fornós"
}
p_person_id = {
    "idperson": 123123,
    "name": "string"
}
p_project_acronym = {
    "idproject": 2323230,
    "acronym": "YEYE"
}
p_project_id = {
    "idproject": 2,
    "acronym": "nothing_good"
}
p_mission = {
    "idmission": 1
}
p_action = {
    "idaction": 1
}
p_location = {
    "idlocation": 1
}
p_featureOfInterest = {
    "idfeatureOfInterest": 1
}
p_observableProperty = {
    "idobservableProperty": 1
}
p_unitOfMeasurement = {
    "idunitOfMeasurement": 1
}
p_observation = {
    "idobservation": 1
}

# URls endpoints
e_auth = 'auth/'
e_physicalSystem = 'physical-system/'
e_person_name = 'person/'
e_person_id = 'person/'
e_project_acronym = 'project/'
e_project_id = 'project/'
e_mission = 'mission/'
e_action = 'action/'
e_location = 'location/'
e_featureOfInterest = 'feature-of-interest/'
e_observableProperty = 'observable-property/'
e_unitOfMeasurement = 'unit-of-measurement/'
e_observation = 'observation/'

dict_endpoint_post = {
    e_auth: p_auth,
    e_physicalSystem: p_physicalSystem,
    e_person_id: p_person_id,
    e_person_name: p_person_name,
    e_project_acronym: p_project_acronym,
    e_project_id: p_project_id,
    e_mission: p_mission,
    e_action: p_action,
    e_location: p_location,
    e_featureOfInterest: p_featureOfInterest,
    e_observableProperty: p_observableProperty,
    e_unitOfMeasurement: p_unitOfMeasurement,
    e_observation: p_observation
}

responses = {}
headers = {'Authorization': 'Innecessari', 'Accept': 'application/json', 'Content-Type': 'application/json'}


def post():
    iteracions = 0
    while iteracions < 1000:
        for endpoint, post_info in dict_endpoint_post.items():
            url = URL + endpoint
            # print(url, post_info)
            r = requests.post(url=url, json=post_info, headers=headers)
            # response_body = r.json()
            response_code = r.status_code
            responses.update({url: response_code})
            iteracions += 1

        # r = requests.post(url = url_observation, json = p_observation)
        # response = r.json()
        # responses.append(response['value'])
        # iteracions += 1

        for url_, response in responses.items():
            if response != 200:
                print(url_, response)


# post()


def get(value=''):
    iteracions = 0
    while iteracions < 1000:
        for endpoint, post_info in dict_endpoint_post.items():
            url = URL + endpoint + value
            # print(url, post_info)
            r = requests.get(url=url)
            try:
                response_body = r.json()
            except:
                pass
            response_code = r.status_code
            responses.update({url: response_code})
            iteracions += 1

        # r = requests.post(url = url_observation, json = p_observation)
        # response = r.json()
        # responses.append(response['value'])
        # iteracions += 1

        for url_, response in responses.items():
            if response != 200:
                print(url_, response)


# get('1')
# get()

import string

list_names = ['aandreu fonos', 'jaquin del riu', 'javi', 'cadenas', 'vidal', 'Neuus', 'neuss vdal', 'maties carnell',
              'matias', 'kram bhail', 'bhail', 'carla', 'crla ateero', 'carla atero delgao']
for x in range(11005):
    char = "".join([random.choice(string.ascii_lowercase) for i in range(random.randint(5, 12))])
    char2 = "".join([random.choice(string.ascii_lowercase) for i in range(random.randint(5, 12))])
    list_names.append(char + ' ' + char2)


def post_person():
    iterations = 0
    url = URL + 'person/'
    code204 = 0
    code210 = 0
    while iterations < 10000:
        r = requests.post(url=url, json={
            "idperson": 1231231,
            "name": list_names[iterations]
        })

        if r.status_code == 204:
            code204 += 1

        elif r.status_code == 210:
            print(list_names[iterations], r.content)
            code210 += 1

        iterations += 1

    print('Code 204:', code204)
    print('Code 210:', code210)
    print('Error:', (code210 / code204) * 100)


post_person()
